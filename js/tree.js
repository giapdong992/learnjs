var dataBegin = {
    name: "Root Tree",
    children: [
        { name: "Empty" },
        {
            name: "Full item",
            children: [
                { name: "Children item1" },
                { name: "Children item2" },
                {
                    name: "Children item n",
                    children: [
                        { name: "Item k" },
                        { name: "Item k+n" }
                    ]
                },
                { name: "Item n + 1" }
            ]
        },
        { name: "Item of die" },
        {
            name: "Chiu chiu",
            children: [
                { name: "Khong co gi" },
                { name: "Pew pew" }
            ]
        }
    ]
}


//Vue for select.html
Vue.component('select-component', {
    template: '#select-home',
    props: {
        data: Object,

    },
    data: function() {
        return {
            demo: 0,
            isOpen: false,
        }
    },
    computed: {
        isFolder() {
            if (this.data.children && this.data.children.length != 0) {
                return true;
            } else return false;
        }
    },
    methods: {
        openFolder() {
            this.isOpen = !this.isOpen;
        },
        makeFolder() {
            if (!this.isFolder) {
                this.$emit('make-folder', this.data);
                this.isOpen = true;
            }
        }
    },
    created: function() {
        console.log("Created select.html");
    }
});
//Vue instantiate

var select = new Vue({
    el: '#select-app',
    data: {
        treeData: dataBegin,
    },
    methods: {
        addItem(parent) {
            var inputItem = prompt("Enter name of Item", "");
            parent.children.push({ name: inputItem });
        },
        makeFolder(event) {
            Vue.set(event, 'children', [])
            this.addItem(event)
        }
    }
});