//Vue for select.html
Vue.component('select-component', {
    template: '#select-home',
    props: {
        columns: Array,
        data: Array,
        filterkey: String,
    },
    data: function() {
        var sortOrder = {};
        this.columns.forEach(function(key) {
            sortOrder[key] = 1;
        });
        //sortOrder[key] >0 => arrow.asc
        return {
            sortKey: '',
            sortOrder: sortOrder,
            toggleControl: true,
            inputMessage: { ID: '', Name: '', Age: '', Phone: '', Facebook: '', Salary: '' },
            itemChoosed: '',
        };
    },
    computed: {
        filterdata: function() {

            var sortKey = this.sortKey
            var filterkey = this.filterkey && this.filterkey.toLowerCase()
            var order = this.sortOrder[sortKey]
            var data = this.data

            //filter follow searchQuery
            if (filterkey) {
                data = data.filter(function(row) {
                    return Object.keys(row).some(function(key) {
                        return String(row[key]).toLowerCase().indexOf(filterkey) > -1
                    })
                })
            }
            //sort
            if (sortKey) {
                //sortKey != null || sortKey != ''
                data = data.slice().sort(function(a, b) {
                    a = a[sortKey]
                    b = b[sortKey]
                    if (sortKey == "Phone" || sortKey == "Salary" || sortKey == "Age") {
                        var temp1 = Number(a);
                        var temp2 = Number(b);
                        return (temp1 - temp2) * order;
                    } else
                        return (a === b ? 0 : a > b ? 1 : -1) * order;
                })
            }
            return data;
        },

    },
    methods: {
        addItem: function() {
            var check = this.checkInput();
            if (check.result && check.message.indexOf("Repeat") < 0) {
                var newItem = {
                    ID: this.inputMessage.ID,
                    Name: this.inputMessage.Name,
                    Age: this.inputMessage.Age,
                    Phone: this.inputMessage.Phone,
                    Facebook: this.inputMessage.Facebook,
                    Salary: this.inputMessage.Salary,

                };
                select.gridData.push(newItem);
                this.resetInput();
            } else if (check.result && check.message.indexOf("Repeat") >= 0) {
                this.haveNote(check.message);
            } else {
                this.haveAlert(check.message);
            }
            //todo
        },
        deleteItem() {
            if (this.itemChoosed) {
                if (confirm("Bạn chắc chứ?")) {
                    var index = -1;
                    select.gridData.forEach(element => {
                        if (element.ID == this.itemChoosed) {
                            index = select.gridData.indexOf(element);
                        }
                    });

                    if (index >= 0) {
                        this.itemChoosed = '';
                        select.gridData.splice(index, 1);
                    }
                }
            } else
                alert("Thôi nào, chọn một item đi chứ!")

        },
        editItem() {
            if (this.itemChoosed) {
                var index = -1;
                select.gridData.forEach(element => {
                    if (element.ID == this.itemChoosed) {
                        index = select.gridData.indexOf(element);
                    }
                }); //Find index of element in select.gridData

                var itemEdit = { ID: '', Name: '', Age: '', Phone: '', Facebook: '', Salary: '' };
                for (const key in itemEdit) {
                    itemEdit[key] = this.inputMessage[key] ? this.inputMessage[key] : select.gridData[index][key];
                }
                var tempIndex = -1;
                select.gridData.forEach(element => {
                    if (element.ID == itemEdit.ID) {
                        tempIndex = select.gridData.indexOf(element);
                    }
                });
                //intex != tempIndex?
                if (index == tempIndex || tempIndex == -1) {
                    Vue.set(select.gridData, index, itemEdit)
                } else
                    alert("ID repeat");

            } else
                alert("Thôi nào, chọn một item đi chứ!")
        },
        sortBy: function(key) {
            this.sortKey = key;
            this.sortOrder[key] = this.sortOrder[key] * -1;
        },
        togglePanel: function() {
            this.toggleControl = !this.toggleControl;
        },
        getToggle: function() {
            if (this.toggleControl) {
                return { "open-toggle": true, "close-toggle": false };
            } else {
                return { "open-toggle": false, "close-toggle": true };
            }
        },
        resetInput: function() {
            var check = this;
            for (const key in check.inputMessage) {
                if (check.inputMessage.hasOwnProperty(key)) {
                    check.inputMessage[key] = '';
                }
            }
        },
        checkInput() {
            var idInput = this.inputMessage;
            var check = true;
            var note = '';
            //validate for repeat ID in select.gridData
            select.gridData.forEach(function(element) {
                if (element.ID == idInput.ID) {
                    check = false;
                    note = "Trùng ID rồi!";
                }
                for (const key in element) {
                    if (element.hasOwnProperty(key)) {
                        if (element[key] == idInput[key]) {
                            note = "Repeat " + key;
                        }
                    }
                }
                //check all for repeat
            });
            for (const key in idInput) {
                if (idInput.hasOwnProperty(key)) {
                    if (idInput[key] == '') {
                        check = false;
                        note = key + " is failed"
                    }
                }
            }
            if (idInput.ID.indexOf(" ") >= 0) {
                check = false;
                note = "Nào nào\nID không có khoảng trắng đâu!";
            }
            var numberAge = isNaN(this.inputMessage.Age);
            if (numberAge) {
                check = false;
                note = "Nhập sai Tuổi rồi!"
            }
            numberAge = isNaN(this.inputMessage.Phone);
            if (numberAge) {
                check = false;
                note = "Nhập sai SĐT rồi!"
            }
            numberAge = isNaN(this.inputMessage.Salary);
            if (numberAge) {
                check = false;
                note = "Nhập sai Lương rồi!"
            }
            if (this.inputMessage.Facebook.indexOf("www.facebook.com") < 0) {
                note = "Repeat " + "Có gì đó sai sai\nLink Facebook phải gồm: www.facebook.com"
            }

            //check = true => input is good
            //check = false => failed input
            return { result: check, message: note };
        },
        haveAlert(avent) {
            alert(avent);
        },
        haveNote(avent) {
            var temp = "Có vẻ như bạn đã trùng: " + avent.slice(7);
            if (confirm(temp)) {
                var newItem = {
                    ID: this.inputMessage.ID,
                    Name: this.inputMessage.Name,
                    Age: this.inputMessage.Age,
                    Phone: this.inputMessage.Phone,
                    Facebook: this.inputMessage.Facebook,
                    Salary: this.inputMessage.Salary,

                };

                select.gridData.push(newItem);
                this.resetInput();
            }
        },
        chooseItem(key) {
            this.itemChoosed = this.itemChoosed == key ? '' : key;
        },



    },
    created: function() {
        console.log("Created select.html");
    }
});
//Vue instantiate

var select = new Vue({
    el: '#select-app',
    data: {
        gridColumns: ["ID", "Name", "Age", "Phone", "Facebook", "Salary"],
        gridData: [
            { ID: "lx", Name: "Giap Dong", Age: 20, Phone: 99871927, Facebook: "www.facebook.com/WinterEpic", Salary: "1000" },
            { ID: "ll", Name: "Giap Duong", Age: 27, Phone: 11242983, Facebook: "www.facebook.com/leuleu", Salary: "2230" },
            { ID: "lv", Name: "Dat Hoang", Age: 29, Phone: 82912782983, Facebook: "www.facebook.com/nanimami", Salary: "1120" },
            { ID: "li", Name: "Tien Dat", Age: 31, Phone: 909293983, Facebook: "www.facebook.com/khongdau", Salary: "2530" },
        ],
        searchQuery: '',

    }
});