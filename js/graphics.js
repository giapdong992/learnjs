// math helper...
function valueToPoint(value, index, total) {
    var y = -value * 0.8
    var angle = Math.PI * 2 / total * index
    var cos = Math.cos(angle)
    var sin = Math.sin(angle)
    var tx = -y * sin + 100
    var ty = y * cos + 100
    return {
        x: tx,
        y: ty
    }
}

var pointTrigger = [
    { label: 'A', value: 100 },
    { label: 'B', value: 100 },
    { label: 'C', value: 100 },
    { label: 'D', value: 100 },
    { label: 'E', value: 100 },
    { label: 'F', value: 100 },
    { label: 'G', value: 100 },
    { label: 'G', value: 100 },
]
Vue.component('graphics', {
    template: '#graphics-template',
    props: ['stats'],
    data: function() {
        return {
            demo: 0,
        }
    },
    computed: {
        points() {
            var arr = [];
            this.stats.forEach(element => {
                var point = valueToPoint(element.value, this.stats.indexOf(element), this.stats.length)
                arr.push(point.x);
                arr.push(point.y);
            });
            return arr.slice().join(',');
        }
    },
    components: {
        // a sub component for the labels
        'axis-label': {
            props: {
                stat: Object,
                index: Number,
                total: Number
            },
            template: '#axis-template',
            computed: {
                point: function() {
                    return valueToPoint(this.stat.value * 1.15,
                        this.index,
                        this.total
                    )
                }
            }
        }
    }
})

Vue.component('table-result', {
    template: '#table-template',
    props: {
        stats: Array,
    },
    computed: {

    }
})
var select = new Vue({
    el: '#select-demo',
    data: {
        statss: pointTrigger,
        newLabel: '',

    },
    computed: {

    },
    methods: {
        remove: function(stat) {
            if (pointTrigger.length > 3) {
                pointTrigger.splice(pointTrigger.indexOf(stat), 1)
            } else {
                alert('Stopppp! Xóa vậy thôi nhé =))')
            }
        },
        add: function(e) {
            e.preventDefault()
            if (!this.newLabel) return
            pointTrigger.push({
                label: this.newLabel,
                value: 100
            })
            this.newLabel = ''
        },
    },
    created() {
        console.log("Created")
    }
})